import "./style.css";

function CardGame() {
  return /*html*/ `
    <figure class="card-game">
      <img src="images/alura-pixel.png" alt="Logo da Alura" class="icon">
    </figure>
  `;
}

export default CardGame;

/* const $root = document.querySelector("#root");
const $htmlCardGame = CardGame();

$root.insertAdjacentElement("beforeend", $htmlCardGame) */
